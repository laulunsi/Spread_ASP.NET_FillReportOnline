Spread for ASP.NET 

全球销量第一的表格控件，类似Excel的强大功能

Spread for ASP.NET表格控件兼容Excel的强大功能，并将其嵌入到您的应用系统中。完备的Excel文档支持使得您可以在企业中分享和访问数据信息；内嵌的图表引擎和数据可视化支持让您更加轻松的为商务、工程以及科学应用系统中创建丰富高效的信息中心。


官网： http://www.gcpowertools.com.cn/products/spread_aspnet.htm

下载： http://www.gcpowertools.com.cn/products/download.aspx?pid=46

社区： http://gcdn.gcpowertools.com.cn/

博客： http://blog.gcpowertools.com.cn/category/Spread.aspx

![葡萄城控件微信服务号](http://www.gcpowertools.com.cn/newimages/qrgrapecity.png "title")

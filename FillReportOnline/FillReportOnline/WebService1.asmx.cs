﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;

namespace AutoCompleteCellType
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string[] GetAllNames(string prefixText, int count)
        {
            ArrayList filteredList = new ArrayList();
            string[] names = { "朱玉辉", "孙娜娜", "杨东波", "蒋国华", "严明磊"};
            foreach (string name in names)
            {
                if (name.ToLower().StartsWith(prefixText.ToLower()))
                    filteredList.Add(name);
            }
            return (string[])filteredList.ToArray(typeof(string));
        }

        
    }
}

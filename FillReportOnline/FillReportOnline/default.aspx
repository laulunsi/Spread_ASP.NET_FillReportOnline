﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="FillReportOnline._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="resource/style/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="resource/script/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
        $.ready = function () {
            //绑定 li 的 click 事件
            $("li").bind("click", { link: this }, changeLink);

            //设置 cookie
            if (document.cookie.indexOf("@link@") < 0) {
                document.cookie = "@link@=" + "adddata.aspx";
            }
            else {
                var cookarray = document.cookie.split(";");
                var link = cookarray[cookarray.length - 1].split("=")[1];
                cookieLink(link);
            }

            $("#childFrame").attr("src", "adddata.aspx");

            setInterval(checkLink, 100);
        }

        function changeLink(events) {

            //var link = this.textContent.trim();
            var link = this.innerHTML;
            if(this.children.length ==2){
                link = this.children[1].innerHTML;
            }
            switch (link.replace(/^\s+|\s+$/g, "")) {
                case "添加订单":
                    $("#childFrame").attr("src", "adddata.aspx");
                    break;
                case "查看报表":
                    $("#childFrame").attr("src", "report.aspx");
                    break;
                case "查看图表":
                    $("#childFrame").attr("src", "chart.aspx");
                    break;
                default:
            }
            document.cookie = "@link@=" + $("#childFrame").attr("src");
        }

        function cookieLink(link) {
            $("#childFrame").attr("src", link);
        }

        function checkLink() {
            var cookarray = document.cookie.split(";");

            for (var i = 0; i < cookarray.length; i++) {
                var name = cookarray[i].split("=")[0];
                var link = cookarray[i].split("=")[1];

                if (name.indexOf("@link@") >= 0 && link == "report.aspx") {
                    $("#designer").css("display", "block");
                }
                else if (name.indexOf("@link@") >= 0 && link != "report.aspx") {
                    $("#designer").css("display", "none");
                }
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 1141px;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="header">
            <div class="header-left">
                <a href="default.aspx">
                    <h1>
                        <img style="border: 0" class="title" src="resource/images/logo-gc-white.png" />
                        <span class="title">在线信息填报系统演示</span>
                    </h1>
                </a>
            </div>
            <div class="header-right">
                <a class="download" target="_blank" href="http://www.gcpowertools.com.cn/products/download.aspx?pid=11">
                    下载试用版
                    <img style="border: 0; padding-top:10px;" src="resource/images/spLogo_256.png" height="100" width="100"/>
                </a>
            </div>
        </div>
        <div class="menu-top">
            <ul>
                <li>添加订单</li>
                <li id="reportli">查看报表</li>
                <li>查看图表</li>
                <li>主题:</li>
                <li style="width: 155px"><span style="padding-top: 9px; display: block;">
                    <asp:DropDownList ID="cbSkins" runat="server" OnSelectedIndexChanged="cbSkins_SelectedIndexChanged">
                    </asp:DropDownList>
                </span></li>
                <li id="designer" style="width: 100px; display: none;"><a href="resource/designer/Spread模板设计器.zip">
                    下载设计器</a> </li>
            </ul>
        </div>
        <div class="content">
            <div class="leftborder">
            </div>
            <div class="menu-left">
                <ul>
                    <li>
                        <img src="resource/images/Button Add.png" style="width: 50px; height: 50px;" /><span>添加订单</span></li>
                    <li>
                        <img src="resource/images/reportviewer.png" style="width: 50px; height: 50px;" /><span>查看报表</span></li>
                    <li>
                        <img src="resource/images/Chart Bar.png" style="width: 50px; height: 50px;" /><span>查看图表</span></li>
                </ul>
            </div>
            <div class="spread">
                <iframe id="childFrame" scrolling="no" class="frame" >
                </iframe>
            </div>
            <div class="leftborder">
            </div>
        </div>
        <div class="footer">
            <span>@2016 GrapeCity 保留所有权利</span>
        </div>
    </div>
    </form>
</body>
</html>
